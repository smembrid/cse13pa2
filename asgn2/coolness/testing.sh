echo no args                                                                
./coolness

echo 1 arg                                                                   
./coolness 30

echo two arguments                                                                     
./coolness 32.5 10

echo temp out of range                                                             
./coolness 355                                                      

echo temp float 
./coolness 5.55 3

echo wind float 
./coolness -10 1.1

echo negative temperature
./coolness -10 5

echo negative wind speed
./coolness 10 -5

echo 0 wind speed
./coolness -10 0

echo both 0
./coolness 0 0

echo out of range
./coolness 55
