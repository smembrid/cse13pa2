#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
    if (argc == 2) {
        // Case when you have one command-line argument (e.g., "./coolness 25.0")
        double T = atof(argv[1]); // Parse the first argument as a double
        if (T != 0.0) {
            if (T >= -10 && T <= 40) {
                for (double V = 5; V <= 15; V += 5) {
                double coolness = 35.74 + 0.6215 * T - 35.75 * pow(V, 0.16) + 0.4275 * T * pow(V, 0.16);
                    printf("%.1f\t%.1f\t%.1f\n", T, V, coolness);
                }
                printf("\n");
            } else {
                fprintf(stderr, "Error: T is not within the range -10 to 40.\n");
                return 1;
            }
        } else {
            fprintf(stderr, "Error: Invalid input for T.\n");
            return 1;
        }

    
    } else if (argc == 3) {
        // Case when you have two command-line arguments (e.g., "./coolness 25.0 10.0")
        double T = atof(argv[1]); // Parse the first argument as a double
        double V = atof(argv[2]); // Parse the second argument as a double
        if (T != 0.0 && V != 0.0) {
            if (T >= -10 && T <= 40 && V >= 5 && V <= 15) {
                double coolness = 35.74 + 0.6215 * T - 35.75 * pow(V, 0.16) + 0.4275 * T * pow(V, 0.16);
                printf("%.1f\t%.1f\t%.1f\n", T, V, coolness);
                printf("\n");
            } else {
                fprintf(stderr, "Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
                return 1;
            }
        } else {
            fprintf(stderr, "Error: Invalid input for T or V.\n");
            return 1;
        }
    } else if (argc > 3) {
        fprintf(stderr, "Usage: ./coolness [temperature] [wind speed]\n");
        return 1;
    } else {
        for (double T = -10; T <= 40; T += 10) {
            for (double V = 5; V <= 15; V += 5) {
                double coolness = 35.74 + 0.6215 * T - 35.75 * pow(V, 0.16) + 0.4275 * T * pow(V, 0.16);
                 printf("%.1f\t%.1f\t%.1f\n", T, V, coolness); // Added a space after each value
            }
            printf("\n"); // Newline after each set of V values
    }
    }

    return 0;
}
