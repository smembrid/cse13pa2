This program is able to calculate “coolness” as a function of wind speed and temperature. 

coolness.c is able to take 0-2 arguments, T (temperature) and V (windspeed)

If coolness.c recieves no arguments, variables T and V are calculated from a range of parameters. The first variable T is taken from range (-10, 40) in increments of 10, and V is taken from range (5,15) in steps of 5. Once calculated, the output should show variable T, V, and the calculated product (for each variable)

If successful, the program should return with exit code 0, and 1 if otherwise. 


If coolness.c receives one argument, variable T will be assigneed to said argument. Variable V will be calculated from a range of parameters, (5,15) in steps of 5. Once calculated, the output should show variable given variable T, V, and the calculated product (for all of V).

If successful, the program should return with exit code 0, and 1 if otherwise. 

If coolness.c receives two arguments, those arguments will be assigned to T and V, respectively. Coolness will be calculated and output will show T, V, and the calculated output. 
If successful, the program should return with exit code 0, and 1 if otherwise. 
