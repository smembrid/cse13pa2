#include <stdio.h>
#include <stdlib.h>

#define MAXIMUM_VALUE 16 

int main() {
    int range = MAXIMUM_VALUE; // Macro MAXIMUM_VALUE, not int max_val
    int histogram[MAXIMUM_VALUE] = {0}; // Histogram array initialized with 0 for all integer values

    int binSize = 1; // size of the bin
    int max_val = 0; // Max value seen so far, not to be confused with MAXIMUM_VALUE
    
    int value;
    // Tell user bin size and range
    printf("%d bins of size %d for range [0,%d)\n", MAXIMUM_VALUE, binSize, range);

    while (scanf("%d", &value) == 1) { // reading in values until EOF found
        
        if (value < 0) continue; // if val is negative, skip it in the loop
        
        while (value >= range) { // If range is exceeded, increase histogram size
            for (int i = 0, j = 0; i < MAXIMUM_VALUE / 2; i++, j += 2) {
                histogram[i] = histogram[j] + histogram[j+1];
            }
        
            range = range * 2;
            binSize = binSize * 2;

            for (int i = MAXIMUM_VALUE / 2; i < MAXIMUM_VALUE; i++) {
                histogram[i] = 0;
            }
            printf("%d bins of size %d for range [0,%d)\n", MAXIMUM_VALUE, binSize, range);
        }
            int bin_index = value / binSize; // Compute bin index for current vaeue
            histogram[bin_index]++;
        }
    

    int iter = 0;
    while (iter < MAXIMUM_VALUE) {
        printf("[%3d:%3d] ", iter * binSize, (binSize - 1) * (iter + 1));
        int curr_val = histogram[iter];
        for (int starCount = 0; starCount < histogram[iter]; starCount++) {
            putchar('*');
        }
        putchar('\n');
        iter++;
    }
    
    return 0;
}
