echo Example 1
echo 3 -4 5 1 7 0 8 0 15 12 3 5 3 3 3 3 3 | ./histogram

echo Example 2
echo 3 -4 5 1 7 0 8 0 15 12 3 5 18 19 20 30 7 12 50 34 32 19 44 | ./histogram

echo Lower than Normal amount of 16
echo {1..15} | ./histogram

echo Small and Big
echo 0 256 | ./histogram

echo Wide number range
echo {1..200} | ./histogram

echo Random positive numbers
echo 7 4 2 1 9 13 6 11 10 | ./histogram

echo Random negative numbers
echo -6 -2 -10 -9 -3 -4 -1 -5 -9 -8 | ./histogram

echo Single value repeated
echo 3 1 1 8 3 1 9 3 2 1 | ./histogram

echo Same values
echo 9 9 9 9 9 9 9 9 9 9 | ./histogram

echo Values below and greater than the maximum
echo 19 15 16 10 13 22 8 10 2 | ./histogram