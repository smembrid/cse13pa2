Histogram.c takes a set of positive integers from standard input (stdin) and prints out a histogram bin sorting out the data. The histogram can scale in relevance to the integers that are inputted into the program. Histogram.c begins with a bin size of 1 and can scale/change based on input - program will double bin size and range. 


The program initiates with a bin size of 1, dynamically doubling as required to encompass all positive input integers, while maintaining precisely 16 bins using an array to track integer counts.






